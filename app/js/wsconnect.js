var bookconnect;

function wsConnect(mode){
	bookconnect = new WebSocket("ws://" + window.location.host + ":10333", "connect");

	bookconnect.onopen = function(e){
        bookconnect.send(JSON.stringify({
            open: true
        }));
    };

    bookconnect.onmessage = function(e){
        // console.log(e.data);    	console.log();
        $("main").children().not("#resultPage").remove();
        $("#resultPage").show();
        // ("main").("<div id='chart'></div>").append(`<table><tbody><tr><td>sadsad</td><td>sadasr</td></tr><tr><td>sadsad</td><td>sadasr</td></tr><tr><td>sadsad</td><td>sadasr</td></tr></tbody></table>`)
        
        var randomized = 1 + Math.floor(Math.random() * 12);

        var parse = JSON.parse(e.data)[randomized];
        console.log(e.data);

        var values = [];
        var labels = [];

        // Emotion

        $.each(parse[0].tones, function(index, obj){
            values.push(parseInt(this.score * 100));
            labels.push(this.tone_name);
        });

        console.log(values);

        
        var data = [{
            values: values,
            labels: labels,
            type: "pie"
        }];

        var layout = {
            height: 450,
            width: 450
        };

        Plotly.newPlot("emotion", data, layout);

        // Language

        var xla = [];
        var yla = [];

        $.each(parse[1].tones, function(index, obj){
            yla.push(parseInt(this.score * 100));
            xla.push(this.tone_name);
        });

        data = [{
            x: xla,
            y: yla,
            type: "bar"
        }];

        Plotly.newPlot("language", data, layout);

        // Social

        xla  = [];
        yla = [];

        $.each(parse[2].tones, function(index, obj){
            yla.push(parseInt(this.score * 100));
            xla.push(this.tone_name);
        });

        data = [{
            x: xla,
            y: yla,
            type: "bar"
        }];

        Plotly.newPlot("social", data, layout);

        // SI - history

        var trace1 = {
            x: [-100, -(values[0])],
            y: [100, (values[0])],
            mode: 'markers',
            name: 'Joy',
            type: 'scatter'
        };

        var trace2 = {
            x: [100, (values[1])],
            y: [100, (values[1])],
            mode: 'markers',
            name: 'Fear',
            type: 'scatter'
        };
        
        var trace3 = {
            x: [-100, -(values[2])],
            y: [-100, -(values[2])],
            mode: 'markers',
            name: 'Anger',
            type: 'scatter'
        };

        var trace4 = {
            x: [100, (values[3])],
            y: [-100, -(values[3])],
            mode: 'markers',
            name: 'Disgust',
            type: 'scatter'
        };

        data = [trace1, trace2, trace3, trace4];
        Plotly.newPlot("history", data, layout);


    };

    bookconnect.onclose = function(e){
    	if (window.wstatus == "loading"){
    		return true;
    	}

        
    }   
}