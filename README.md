

Booklyze
=======
*The Jailers' project/concept for Kovork's HACKATHON V.*

Booklyze is a Node.js-based webapplication (tool or social platform) for mood and taxonomy analysis of books (PDF, Mobi, EPUB).


----------

You will need
-------

- Nginx
- Latest Node.js
- IBM Watson API key

List of components 
----------
 - IBM Watson: Tone Analyzer module
 - Firebase (user database)
 - Plotly library for JS (charts)
 - pdf-to-text module for Node.js